﻿using Vboot.Core.Module.Bpm;

namespace Vboot.Extend.Oa;

[SugarTable("oa_flow_main", TableDescription = "OA流程实例表")]
[Description("OA流程实例表")]
public class OaFlowMain : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "全局流程模板ID", IsNullable = true, Length = 32)]
    public string protd { get; set; }
    
    [SugarColumn(ColumnDescription = "表单数据", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string zform { get; set; }
    
    [SugarColumn(ColumnDescription = "全局流程模板ID", IsNullable = true, Length = 32)]
    public string temid { get; set; }
    
    [SugarColumn(IsIgnore = true)] public string prxml { get; set; }

    
    [SugarColumn(IsIgnore = true)] 
    public Zbpm zbpm { get; set; }
    
    [SugarColumn(ColumnDescription = "状态", IsNullable = true, Length = 8)]
    public string state { get; set; }
    
    
    

}