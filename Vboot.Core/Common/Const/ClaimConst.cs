﻿namespace Vboot.Core.Common;

public class ClaimConst
{
    /// <summary>
    /// 用户Id
    /// </summary>
    public const string CLAINM_USERID = "UserId";

    /// <summary>
    /// 账号
    /// </summary>
    public const string CLAINM_ACCOUNT = "Account";

    /// <summary>
    /// 名称
    /// </summary>
    public const string CLAINM_NAME = "Name";

    /// <summary>
    /// 是否超级管理
    /// </summary>
    public const string CLAINM_SUPERADMIN = "SuperAdmin";

    /// <summary>
    /// 是否超级管理
    /// </summary>
    public const string TENANT_ID = "TenantId";

    public const string CLAINM_PERMS = "Perms";
    
    public const string CLAINM_DEPTID = "DeptId";
    
    public const string CLAINM_TYPE = "Type";
    
    public const string CLAINM_LABEL = "Label";
    
    public const string CLAINM_CONDS = "Conds";
}