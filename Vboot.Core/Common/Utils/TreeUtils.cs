﻿namespace Vboot.Core.Common;

public class TreeUtils
{
    public static List<Ztree> Build(List<Ztree> nodes)
    {
        List<Ztree> list = new List<Ztree>();
        foreach (var node in nodes)
        {
            if (node.pid == null)
            {
                list.Add(findZtreeChildrenByTier(node, nodes));
            }
            else
            {
                bool flag = false;
                foreach (var node2 in nodes)
                {
                    if (node.pid == (node2.id))
                    {
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                {
                    list.Add(findZtreeChildrenByTier(node, nodes));
                }
            }
        }

        return list;
    }

    //递归查找子节点
    private static Ztree findZtreeChildrenByTier(Ztree node, List<Ztree> nodes)
    {
        foreach (var item in nodes)
        {
            if (node.id == item.pid)
            {
                if (node.children == null)
                {
                    node.children = new List<Ztree>();
                }

                node.children.Add(findZtreeChildrenByTier(item, nodes));
            }
        }

        return node;
    }
    
    //.net的有bug（子对象缺失属性），java的可以
    // public static List<T> BuildEtree<T>(List<T> nodes) where T : Ztree
    // {
    //     List<T> list = new List<T>();
    //     foreach (var node in nodes)
    //     {
    //         if (node.pid == null)
    //         {
    //             list.Add(findEtreeChildrenByTier(node, nodes));
    //         }
    //         else
    //         {
    //             bool flag = false;
    //             foreach (var node2 in nodes)
    //             {
    //                 if (node.pid == (node2.id))
    //                 {
    //                     flag = true;
    //                     break;
    //                 }
    //             }
    //             if (!flag)
    //             {
    //                 list.Add(findEtreeChildrenByTier(node, nodes));
    //             }
    //         }
    //     }
    //
    //     return list;
    // }

    //递归查找子节点
    // private static T findEtreeChildrenByTier<T>(T node, List<T> nodes) where T : Ztree
    // {
    //     foreach (var item in nodes)
    //     {
    //         if (node.id == item.pid)
    //         {
    //             if (node.children == null)
    //             {
    //                 node.children = new List<Ztree>();
    //             }
    //             node.children.Add(findEtreeChildrenByTier(item, nodes));
    //         }
    //     }
    //     return node;
    // }
}