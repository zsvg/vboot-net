﻿namespace Vboot.Core.Common;

public class Zres
{
    public int code { get; set; }

    public string msg { get; set; }
    
    public object data { get; set; }
}