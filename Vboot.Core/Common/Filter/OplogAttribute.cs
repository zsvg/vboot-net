﻿namespace Vboot.Core.Common;

/// <summary>
/// 操作日志特性
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class OplogAttribute : Attribute
{
    /// <summary>
    /// 构造函数
    /// </summary>
    public OplogAttribute()
    {
        
    }

    public string name { get; set; }

    public OplogAttribute(string name) => this.name = name;

}