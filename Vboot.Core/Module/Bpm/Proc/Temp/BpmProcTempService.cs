﻿namespace Vboot.Core.Module.Bpm;

public class BpmProcTempService : BaseMainService<BpmProcTemp>, ITransient
{
    public BpmProcTempService(SqlSugarRepository<BpmProcTemp> repo)
    {
        this.repo = repo;
    }
}