﻿namespace Vboot.Core.Module.Bpm;

public class Zcond
{
    public string modty { get; set; }//业务模型类型
    
    public string proid { get; set; }//流程实例ID
    
    public string temid { get; set; }//流程模板ID
}