﻿namespace Vboot.Core.Module.Bpm;

[SugarTable("bpm_proc_cond", TableDescription = "流程参数表")]
public class BpmProcCond
{
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true, Length = 36)]
    public string id { get; set; }


    [SugarColumn(ColumnDescription = "条件参数", IsNullable = true, Length = 2000)]
    public string cond { get; set; }
}