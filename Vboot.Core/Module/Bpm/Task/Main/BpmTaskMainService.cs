﻿namespace Vboot.Core.Module.Bpm;

public class BpmTaskMainService : ITransient
{
    public async Task<List<BpmTaskMain>> FindAllByProidNotActive(string proid)
    {
        return await _repo.Context.Queryable<BpmTaskMain>()
            .Where(t => t.actag == false)
            .Where(t => t.proid == proid)
            .OrderBy(t => t.ornum)
            .ToListAsync();
    }

    public async Task<List<BpmTaskMain>> FindAllByProid(string proid)
    {
        return await _repo.Context.Queryable<BpmTaskMain>()
            .Where(t => t.proid == proid)
            .OrderBy(t => t.ornum)
            .ToListAsync();
    }

    public async Task<BpmTaskMain> CreateTask(Zbpm zbpm, Znode znode)
    {
        BpmTaskMain task = new BpmTaskMain();
        task.id = YitIdHelper.NextId() + "";
        task.proid = zbpm.proid;
        task.state = "20";
        task.exman = znode.exmen;
        task.ornum = 0;
        task.actag = true;
        task.nodid = znode.nodid;
        task.type = znode.facty;
        await _repo.InsertAsync(task);
        return task;
    }

    public async Task<List<BpmTaskMain>> CreateTaskList(Zbpm zbpm, Znode znode)
    {
        List<BpmTaskMain> list = new List<BpmTaskMain>();
        if (!string.IsNullOrEmpty(znode.exmen) && !znode.exmen.Contains(";"))
        {
            BpmTaskMain task = new BpmTaskMain();
            task.id = YitIdHelper.NextId() + "";
            task.proid = zbpm.proid;
            task.state = "20";
            task.exman = znode.exmen;
            task.ornum = 0;
            task.actag = true;
            task.nodid = znode.nodid;
            task.type = znode.facty;
            await _repo.InsertAsync(task);
            list.Add(task);
        }
        else if ("1" == znode.flway)
        {
            string[] ids = znode.exmen.Split(";");
            for (int i = 0; i < ids.Length; i++)
            {
                BpmTaskMain task = new BpmTaskMain();
                task.id = YitIdHelper.NextId() + "";
                task.proid = zbpm.proid;
                task.state = "20";
                task.exman = ids[i];
                task.ornum = i;
                if (i == 0)
                {
                    task.actag = true;
                }
                else
                {
                    task.actag = false;
                }

                task.nodid = znode.nodid;
                task.type = znode.facty;
                await _repo.InsertAsync(task);
                list.Add(task);
            }
        }
        else if ("2" == znode.flway || "3" == znode.flway)
        {
            string[] ids = znode.exmen.Split(";");
            for (int i = 0; i < ids.Length; i++)
            {
                BpmTaskMain task = new BpmTaskMain();
                task.id = YitIdHelper.NextId() + "";
                task.proid = zbpm.proid;
                task.state = "20";
                task.exman = ids[i];
                task.ornum = i;
                task.actag = true;
                task.nodid = znode.nodid;
                task.type = znode.facty;
                await _repo.InsertAsync(task);
                list.Add(task);
            }
        }

        return list;
    }

    public async Task FindCurrentExmen(List<dynamic> itemList)
    {
        string ids = "(";
        foreach (var item in itemList)
        {
            if (item.state != "30")
            {
                ids += "'" + item.id + "',";
            }
        }

        if ("(" != ids)
        {
            ids = ids.Substring(0, ids.Length - 1) + ")";
            string sql =
                "select n.id as \"tasid\",t.id as \"nodid\",o.name \"exnam\",n.exman \"exman\",t.proid \"proid\"," +
                "t.facno \"facno\",t.facna \"facna\" from bpm_node_main t" +
                " inner join bpm_task_main n on n.nodid=t.id " +
                "inner join sys_org o on o.id=n.exman " +
                "where t.proid in " + ids + " and n.actag=1 order by t.proid, n.ornum";
            List<dynamic> tasks = await _repo.Context.Ado.SqlQueryAsync<dynamic>(sql);

            List<ZidNamePid> list = new List<ZidNamePid>();
            string proid = "";
            foreach (var task in tasks)
            {
                if (task.proid != proid)
                {
                    ZidNamePid zinp = new ZidNamePid();
                    zinp.id = task.proid;
                    zinp.name = task.facno + "." + task.facna;
                    zinp.pid = task.exnam;
                    list.Add(zinp);
                }
                else
                {
                    list[list.Count - 1].pid += ";" + task.exnam;
                }

                proid = task.proid;
            }

            foreach (dynamic item in itemList)
            {
                foreach (var zinp in list)
                {
                    if (zinp.id == item.id)
                    {
                        item.facno = zinp.name;
                        item.exmen = zinp.pid;
                        break;
                    }
                }
            }
        }
    }

    public async Task<BpmTaskMain> FindOne(string id)
    {
       return await _repo.GetByIdAsync(id);
    }

    public async Task Update(BpmTaskMain main)
    {
         await _repo.UpdateAsync(main);
    }
    
    public async Task Delete(string id)
    {
        await _repo.Context.Deleteable<BpmTaskMain>().Where(it => it.id == id).ExecuteCommandAsync();
    }
    
    public async Task DeleteAllByProid(string proid) {
        await _repo.Context.Deleteable<BpmTaskMain>().Where(it => it.proid == proid).ExecuteCommandAsync();
    }

    private readonly SqlSugarRepository<BpmTaskMain> _repo;

    public BpmTaskMainService(SqlSugarRepository<BpmTaskMain> repo)
    {
        _repo = repo;
    }
}