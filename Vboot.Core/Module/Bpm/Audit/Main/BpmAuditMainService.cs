﻿namespace Vboot.Core.Module.Bpm;

public class BpmAuditMainService : ITransient
{
    public async Task<BpmAuditMain> SaveDraftAudit(Zbpm zbpm, Znode znode)
    {
        BpmAuditMain audit = new BpmAuditMain();
        audit.id = YitIdHelper.NextId() + "";
        audit.facno = znode.facno;
        audit.facna = znode.facna;
        audit.nodid = znode.nodid;
        audit.haman = zbpm.haman;
        audit.proid = zbpm.proid;
        audit.opnot = zbpm.opnot;
        audit.atids = zbpm.atids;
        audit.opkey = "dsubmit";
        audit.opinf = "起草人提交";
        await _repo.InsertAsync(audit);
        return audit;
    }

    public async Task<BpmAuditMain> SaveAudit(Zbpm zbpm)
    {
        BpmAuditMain audit = new BpmAuditMain();
        audit.id = YitIdHelper.NextId() + "";
        audit.facno = zbpm.facno;
        audit.facna = zbpm.facna;
        audit.nodid = zbpm.nodid;
        audit.haman = zbpm.haman;
        audit.proid = zbpm.proid;
        audit.opnot = zbpm.opnot;
        audit.opkey = zbpm.opkey;
        audit.opinf = zbpm.opinf;
        audit.tasid = zbpm.tasid;
        audit.atids = zbpm.atids;
        await _repo.InsertAsync(audit);
        return audit;
    }

    public async Task<BpmAuditMain> SaveEndAudit(Zbpm zbpm, string nodid)
    {
        BpmAuditMain audit = new BpmAuditMain();
        audit.id = YitIdHelper.NextId() + "";
        audit.facno = "NE";
        audit.facna = "结束节点";
        audit.nodid = nodid;
        audit.proid = zbpm.proid;
        audit.opkey = "end";
        await _repo.InsertAsync(audit);
        return audit;
    }

    private readonly SqlSugarRepository<BpmAuditMain> _repo;

    public BpmAuditMainService(SqlSugarRepository<BpmAuditMain> repo)
    {
        _repo = repo;
    }
}