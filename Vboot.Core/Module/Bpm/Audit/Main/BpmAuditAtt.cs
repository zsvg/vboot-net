﻿namespace Vboot.Core.Module.Bpm;

[SugarTable("bpm_audit_att", TableDescription = "流程评审附件")]
public class BpmAuditAtt
{
    [SugarColumn(ColumnDescription = "主键", IsPrimaryKey = true, Length = 32)]
    public string id { get; set; }
    
    [SugarColumn(ColumnDescription = "文件全名", IsNullable = true, Length = 255)]
    public string name { get; set; }

    [SugarColumn(ColumnDescription = "地址", IsNullable = true, Length = 255)]
    public string path { get; set; }

    [SugarColumn(ColumnDescription = "文件ID", IsNullable = true, Length = 32)]
    public string filid { get; set; }  
    
    [SugarColumn(ColumnDescription = "评审ID", IsNullable = true, Length = 32)]
    public string audid { get; set; }

    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }
    
}