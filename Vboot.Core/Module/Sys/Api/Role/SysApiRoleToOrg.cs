﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_api_role_org", TableDescription = "接口角色组织对应表")]
public class SysApiRoleToOrg
{
    [SugarColumn(ColumnDescription = "角色ID", IsNullable = true,Length = 32)]
    public string rid { get; set; }

    [SugarColumn(ColumnDescription = "组织架构ID", IsNullable = true,Length = 32)]
    public string oid { get; set; }
}