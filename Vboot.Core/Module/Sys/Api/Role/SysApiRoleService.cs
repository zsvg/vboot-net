﻿namespace Vboot.Core.Module.Sys;

public class SysApiRoleService : BaseMainService<SysApiRole>, ITransient
{
    public SysApiRoleService(SqlSugarRepository<SysApiRole> repo)
    {
        base.repo = repo;
    }

    public async Task InsertAsync(SysApiRole role, List<SysApiRoleToOrg> romaps, List<SysApiRoleToApi> rmmaps)
    {
        repo.Context.Ado.BeginTran();
        await base.InsertAsync(role);
        await repo.Context.Insertable(romaps).UseParameter().ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).UseParameter().ExecuteCommandAsync();
        repo.Context.Ado.CommitTran();
    }

    public async Task UpdateAsync(SysApiRole role, List<SysApiRoleToOrg> romaps, List<SysApiRoleToApi> rmmaps)
    {
        repo.Context.Ado.BeginTran();
        await base.UpdateAsync(role);
        await repo.Context.Deleteable<SysApiRoleToOrg>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(romaps).UseParameter().ExecuteCommandAsync();
        await repo.Context.Deleteable<SysApiRoleToApi>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).UseParameter().ExecuteCommandAsync();
        repo.Context.Ado.CommitTran();
    }
}