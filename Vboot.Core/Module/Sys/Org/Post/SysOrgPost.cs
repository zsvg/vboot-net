﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_org_post", TableDescription = "系统岗位表")]
[Description("系统岗位表")]
public class SysOrgPost : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "部门ID", IsNullable = true, Length = 36)]
    public string depid { get; set; }

    [SugarColumn(IsIgnore = true)] public SysOrgDept dept { get; set; }

    [SugarColumn(ColumnDescription = "层级", IsNullable = true, Length = 1000)]
    public string tier { get; set; }

    [SugarColumn(ColumnDescription = "标签", IsNullable = true, Length = 32)]
    public string label { get; set; }
    
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }


    [SugarColumn(ColumnDescription = "ldap层级名称", IsNullable = true, Length = 1000)]
    public string ldnam { get; set; }

    [Navigate(typeof(SysOrgPostOrg), nameof(SysOrgPostOrg.pid), nameof(SysOrgPostOrg.oid))]
    [SugarColumn(IsIgnore = true)] public List<SysOrg> users { get; set; }
}