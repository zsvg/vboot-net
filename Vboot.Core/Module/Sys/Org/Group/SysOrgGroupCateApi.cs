﻿namespace Vboot.Core.Module.Sys;

public class SysOrgGroupCateApi : IDynamicApiController
{
    private readonly SysOrgGroupCateService _service;

    public SysOrgGroupCateApi(SysOrgGroupCateService service)
    {
        _service = service;
    }

   /// <summary>
    /// 获取所有分类-用于分类选择下拉框
    /// </summary>
    [QueryParameters]
    public async Task<dynamic> GetTreea()
    {
        var treeList = await _service.repo.Context
            .Queryable<SysOrgGroupCate>().OrderBy(it => it.ornum)
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }

    /// <summary>
    /// 获取除自身及子节点外的分类
    /// </summary>
    [QueryParameters]
    public async Task<dynamic> GetTreez(string id)
    {
        var treeList = await _service.repo.Context
            .Queryable<SysOrgGroupCate>()
            .WhereIF(!string.IsNullOrWhiteSpace(id), t => t.id != id)
            .OrderBy(it => it.ornum)
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }

    /// <summary>
    /// 获取分类treeTable数据
    /// </summary>
    [QueryParameters]
    public async Task<dynamic> GetTree()
    {
        var treeList = await _service.repo.Context
            .Queryable<SysOrgGroupCate>()
            .OrderBy(it => it.ornum)
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }

    /// <summary>
    /// 获取单个分类详细信息
    /// </summary>
    public async Task<SysOrgGroupCate> GetOne(string id)
    {
        var cate = await _service.SingleAsync(id);
        if (cate.pid != null)
        {
            cate.pname = await _service.repo.Context.Queryable<SysOrgGroupCate>()
                .Where(it => it.id == cate.pid).Select(it => it.name).SingleAsync();
        }

        return cate;
    }

    /// <summary>
    /// 新增分类
    /// </summary>
    public async Task<string> Post(SysOrgGroupCate cate)
    {
        int count = await _service.repo.Context.Queryable<SysOrgGroupCate>().Where(t => t.pid == cate.pid).CountAsync();
        cate.ornum = ++count;
        return await _service.InsertAsync(cate);
    }

    /// <summary>
    /// 更新分类
    /// </summary>
    public async Task<string> Put(SysOrgGroupCate cate)
    {
        return await _service.UpdateAsync(cate);
    }

    /// <summary>
    /// 删除分类
    /// </summary>
    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        foreach (var id in idArr)
        {
            var cateCount = await
                _service.repo.Context.Queryable<SysOrgGroupCate>().Where(it => it.pid == id).CountAsync();
            if (cateCount > 0)
            {
                throw new Exception("此分类下有子分类，无法删除");
            }

            var mainCount = await
                _service.repo.Context.Queryable<SysOrgGroup>().Where(it => it.catid == id).CountAsync();

            if (mainCount > 0)
            {
                throw new Exception("此分类下有群组，无法删除");
            }
        }

        await _service.DeleteAsync(ids);
    }

    /// <summary>
    /// 移动分类
    /// </summary>
    public async Task PostMove(TreeMovePo po)
    {
        await _service.Move(po);
    }
}