﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_org_group", TableDescription = "组织机构群组表")]
[Description("组织机构群组表")]
public class SysOrgGroup : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "标签", IsNullable = true, Length = 32)]
    public string label { get; set; }

    [SugarColumn(ColumnDescription = "分类ID", IsNullable = true, Length = 32)]
    public string catid { get; set; }
    
    [Navigate(typeof(SysOrgGroupOrg), nameof(SysOrgGroupOrg.gid), nameof(SysOrgGroupOrg.oid))]
    [SugarColumn(IsIgnore = true)] public List<SysOrg> members { get; set; }
}