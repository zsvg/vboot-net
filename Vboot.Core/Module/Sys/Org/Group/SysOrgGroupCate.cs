﻿namespace Vboot.Core.Module.Sys;


[SugarTable("sys_org_group_cate", TableDescription = "组织架构群组分类")]
public class SysOrgGroupCate : BaseCateEntity
{
    /// <summary>
    /// 子分类集合
    /// </summary>
    [SugarColumn(IsIgnore = true)] 
    public List<SysOrgGroupCate> children { get; set; }
    
}