﻿namespace Vboot.Core.Module.Sys;

[ApiDescriptionSettings("Sys", Tag = "组织架构-角色树")]
public class SysOrgRtreeApi: IDynamicApiController
{
    private readonly SysOrgRoleTreeService _service;

    public SysOrgRtreeApi(SysOrgRoleTreeService service)
    {
        _service = service;
    }
    
    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<SysOrgRoleTree>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .Select((t) => new {t.id, t.name, t.notes, t.crtim, t.uptim})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<SysOrgRoleTree> GetOne(string id)
    {
        return await _service.Select(id);
    }

    public async Task Post(SysOrgRoleTree cate)
    {
        await _service.Insertx(cate);
    }

    public async Task Put(SysOrgRoleTree cate)
    {
        await _service.Updatex(cate);
    }
    
    public async Task Delete(string ids)
    {
        await _service.Deletex(ids);
    }
    
    [QueryParameters]
    public async Task<SysOrg> GetCalc(string useid,string rolid)
    {
        return await _service.calc(useid, rolid);
    }
    
    [QueryParameters]
    public async Task<List<SysOrgRoleTree>> GetTlist(string id)
    {
        return await _service.repo.Context.Queryable<SysOrgRoleTree>()
            .ToListAsync();
    }
    
    [QueryParameters]
    public async Task<List<SysOrgRole>> GetRlist(string treid)
    {
        return await _service.repo.Context.Queryable<SysOrgRole>()
            .Where(t=>t.treid==treid)
            .ToListAsync();
    }
}