﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_org_role_tree", TableDescription = "组织架构角色树")]
[Description("组织架构角色树")]
public class SysOrgRoleTree : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }
    
    /// <summary>
    /// 角色列表
    /// </summary>
    [Navigate(NavigateType.OneToMany, nameof(SysOrgRole.treid))]
    [SugarColumn(IsIgnore = true)]
    public List<SysOrgRole> roles { get; set; }
}