﻿namespace Vboot.Core.Module.Sys;

public class SysOrgRoleTreeService: BaseMainService<SysOrgRoleTree>, ITransient
{
    
    public async Task Insertx(SysOrgRoleTree main)
    {
     
        if (string.IsNullOrEmpty(main.id))
        {
            main.id = YitIdHelper.NextId()+"";
            main.crmid = XuserUtil.getUserId();
        }
        await repo.Context.InsertNav(main)
            .Include(it => it.roles)
            .ExecuteCommandAsync();
        List<SysOrg> list = new List<SysOrg>();
        foreach (var role in main.roles)
        {
            SysOrg sysOrg = new SysOrg(role.id, role.name, 32);
            list.Add(sysOrg);
        }
        await _sysOrgRepo.InsertOrUpdateAsync(list);
    }
    
    public async Task Updatex(SysOrgRoleTree main)
    {
        main.upmid = XuserUtil.getUserId();
        main.uptim = DateTime.Now;
        await repo.Context.UpdateNav(main)
            .Include(it => it.roles)
            .ExecuteCommandAsync();
        List<SysOrg> list = new List<SysOrg>();
        foreach (var role in main.roles)
        {
            SysOrg sysOrg = new SysOrg(role.id, role.name, 32);
            list.Add(sysOrg);
        }
        await _sysOrgRepo.InsertOrUpdateAsync(list);
    }
    
    public async Task Deletex(string ids)
    {
        var idArr = ids.Split(",");
        for (int i = 0; i < idArr.Length; i++)
        {
            var id = idArr[i];
            await repo.Context.DeleteNav<SysOrgRoleTree>(it => it.id == id)
                .Include(it => it.roles)
                .ExecuteCommandAsync();
        }
    }
    
    //级联查询
    public async Task<SysOrgRoleTree> Select(string id)
    {
        var main = await repo.Context.Queryable<SysOrgRoleTree>()
            .Where(it => it.id == id)
            .Includes(t => t.crman)
            .Includes(t => t.upman)
            .Includes(t => t.roles)
            .SingleAsync();
        return main;
    }
    
    public async Task<SysOrg> calc(string useid, string rolid) {
        string sql = "select t.tier \"tier\",m.ornum \"ornum\" from sys_org_role_node t " +
                     "inner join sys_org_role m on m.treid=t.treid where t.memid=@useid and m.id=@rolid";
        dynamic map =await repo.Context.Ado.SqlQuerySingleAsync<dynamic>(sql,new {useid,rolid});
        if (map == null) {
            sql = "select t.tier \"tier\",m.ornum \"ornum\" from sys_org_role_node t " +
                  "inner join sys_org_role m on m.treid=t.treid " +
                  "inner join sys_org_user u on u.depid=t.memid where u.id=@useid and m.id=@rolid";
            map =await repo.Context.Ado.SqlQuerySingleAsync<dynamic>(sql,new {useid,rolid});
            if (map == null) {
                return null;
            }
        }
        string tier = "" + map.tier;
        int ornum = Convert.ToInt32("" + map.ornum);
        string[] idArr = tier.Split("x");
        string sql3 = "select o.id,o.name,o.type from sys_org_role_node t inner join sys_org o on o.id=t.memid where t.id=@id";
        SysOrg org= await repo.Context.Ado.SqlQuerySingleAsync<SysOrg>(sql3,new {id=idArr[ornum]});
        return org;
    }
    
    private readonly SqlSugarRepository<SysOrg> _sysOrgRepo;

    
    public SysOrgRoleTreeService(SqlSugarRepository<SysOrgRoleTree> repo,
        SqlSugarRepository<SysOrg> sysOrgRepo)
    {
        base.repo = repo;
        _sysOrgRepo = sysOrgRepo;
    }
    
}