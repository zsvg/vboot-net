﻿namespace Vboot.Core.Module.Sys;

public class SysPortalMenuService : BaseMainService<SysPortalMenu>, ITransient
{
    public SysPortalMenuService(SqlSugarRepository<SysPortalMenu> repo)
    {
        base.repo = repo;
    }
}