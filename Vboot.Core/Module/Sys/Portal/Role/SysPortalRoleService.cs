﻿namespace Vboot.Core.Module.Sys;

public class SysPortalRoleService : BaseMainService<SysPortalRole>, ITransient
{
    public SysPortalRoleService(SqlSugarRepository<SysPortalRole> repo)
    {
        base.repo = repo;
    }

    public async Task InsertAsync(SysPortalRole role, List<SysPortalRoleToOrg> romaps, List<SysPortalRoleToMenu> rmmaps)
    {
        repo.Context.Ado.BeginTran();
        await base.InsertAsync(role);
        await repo.Context.Insertable(romaps).UseParameter().ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).UseParameter().ExecuteCommandAsync();
        repo.Context.Ado.CommitTran();
    }

    public async Task UpdateAsync(SysPortalRole role, List<SysPortalRoleToOrg> romaps, List<SysPortalRoleToMenu> rmmaps)
    {
        repo.Context.Ado.BeginTran();
        await base.UpdateAsync(role);
        await repo.Context.Deleteable<SysPortalRoleToOrg>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(romaps).UseParameter().ExecuteCommandAsync();
        await repo.Context.Deleteable<SysPortalRoleToMenu>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).UseParameter().ExecuteCommandAsync();
        repo.Context.Ado.CommitTran();
    }
}