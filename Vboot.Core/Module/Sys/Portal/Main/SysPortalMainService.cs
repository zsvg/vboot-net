﻿namespace Vboot.Core.Module.Sys;

public class SysPortalMainService : BaseMainService<SysPortalMain>, ITransient
{
    public SysPortalMainService(SqlSugarRepository<SysPortalMain> repo)
    {
        this.repo = repo;
    }
}