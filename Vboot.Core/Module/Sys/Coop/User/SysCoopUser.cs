﻿namespace Vboot.Core.Module.Sys;

/// <summary>
/// 合作公司用户信息
/// </summary> 
[SugarTable("sys_coop_user", TableDescription = "合作公司用户信息")]
public class SysCoopUser : BaseMainEntity
{
    /// <summary>
    /// 公司ID
    /// </summary> 
    [SugarColumn( ColumnDescription = "公司ID", IsNullable = true, Length = 36)]
    public string corid { get; set; }
    
    [SugarColumn(IsIgnore = true)] public SysCoopCorp corp { get; set; }
    
    //区分主账号或其他用途
    [SugarColumn(ColumnDescription = "账号类型", IsNullable = true, Length = 32)]
    public string type { get; set; }
    
    [SugarColumn(ColumnDescription = "对接人", IsNullable = true, Length = 32)]
    public string doman { get; set; }
    
    [SugarColumn(ColumnDescription = "用户名", IsNullable = true, Length = 32)]
    public string usnam { get; set; }
    
    // [JsonIgnore]
    [SugarColumn(ColumnDescription = "密码", IsNullable = true, Length = 64, IsOnlyIgnoreUpdate = true)]
    public string pacod { get; set; }

    [SugarColumn(ColumnDescription = "邮箱", IsNullable = true, Length = 64)]
    public string email { get; set; }

    [SugarColumn(ColumnDescription = "手机号", IsNullable = true, Length = 32)]
    public string monum { get; set; }

    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "最后登录时间", IsNullable = true)]
    public DateTime lalot { get; set; }

    [SugarColumn(ColumnDescription = "最后登录IP", IsNullable = true, Length = 20)]
    public string laloi { get; set; }
    
    [SugarColumn(ColumnDescription = "层级", IsNullable = true, Length = 1000)]
    public string tier { get; set; }
    
    [SugarColumn(ColumnDescription = "通知阅读记录", IsNullable = true, Length = 32)]
    public string relog { get; set; }

    [SugarColumn(ColumnDescription = "缓存标记", IsNullable = true)]
    public bool catag { get; set; }

}