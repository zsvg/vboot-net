﻿using Vboot.Core.Module.Bpm;

namespace Vboot.Core.Module.Sys;

/// <summary>
/// 外部公司
/// </summary> 
[SugarTable("sys_coop_corp", TableDescription = "外部公司")]
public class SysCoopCorp : BaseMainEntity
{
    [SugarColumn( ColumnDescription = "分类ID", IsNullable = true, Length = 36)]
    public string catid { get; set; }

    [SugarColumn(ColumnDescription = "流水号", IsNullable = true, Length = 32)]
    public string senum { get; set; }

    [SugarColumn(ColumnDescription = "状态", IsNullable = true, Length = 8)]
    public string state { get; set; }
    
    [SugarColumn(ColumnDescription = "销售机构ID", IsNullable = true, Length = 32)]
    public string braid { get; set; }
    
    [SugarColumn(IsIgnore = true)]
    public string brana{ get; set; }
    
    //地址选择示例，可根据实际情况存想要的字段
    //region-----地址相关信息-----
    /// <summary>
    /// 完整地址
    /// </summary>
    [SugarColumn(ColumnDescription = "完整地址", IsNullable = true, Length = 255)]
    public string addre { get; set; }

    /// <summary>
    /// 省市区
    /// </summary>
    [SugarColumn(ColumnDescription = "省市区", IsNullable = true, Length = 64)]
    public string adreg { get; set; }

    /// <summary>
    /// 省市区以外的详细信息
    /// </summary> 
    [SugarColumn(ColumnDescription = "省市区以外的详细信息", IsNullable = true, Length = 128)]
    public string addet { get; set; }

    /// <summary>
    /// 经纬度
    /// </summary> 
    [SugarColumn(ColumnDescription = "经纬度", IsNullable = true, Length = 32)]
    public string adcoo { get; set; }

    /// <summary>
    /// 省
    /// </summary> 
    [SugarColumn(ColumnDescription = "省", IsNullable = true, Length = 32)]
    public string adpro { get; set; }

    /// <summary>
    /// 市
    /// </summary> 
    [SugarColumn(ColumnDescription = "市", IsNullable = true, Length = 32)]
    public string adcit { get; set; }

    /// <summary>
    /// 区
    /// </summary> 
    [SugarColumn(ColumnDescription = "区", IsNullable = true, Length = 32)]
    public string addis { get; set; }
    //endregion
    
    [SugarColumn(ColumnDescription = "税务登记号", IsNullable = true, Length = 32)]
    public string renum { get; set; }
    
    [SugarColumn(ColumnDescription = "法人代表", IsNullable = true, Length = 32)]
    public string leman { get; set; }
    
    [SugarColumn(ColumnDescription = "公司电话", IsNullable = true, Length = 32)]
    public string cotel { get; set; }   
    
    [SugarColumn(ColumnDescription = "公司传真", IsNullable = true, Length = 32)]
    public string cofax { get; set; }  
    
    [SugarColumn(ColumnDescription = "成立日期", IsNullable = true, Length = 32)]
    public string redat { get; set; }
    
    [SugarColumn(ColumnDescription = "注册资金（万）", IsNullable = true, Length = 32)]
    public string recap { get; set; }
    
    [SugarColumn(ColumnDescription = "邮编", IsNullable = true, Length = 32)]
    public string cozip { get; set; }   
    
    [SugarColumn(ColumnDescription = "公司邮箱", IsNullable = true, Length = 32)]
    public string comai { get; set; }
    
    [SugarColumn(ColumnDescription = "关联企业", IsNullable = true, Length = 255)]
    public string licos { get; set; }
    
    [SugarColumn(ColumnDescription = "备用字段1", IsNullable = true, Length = 64)]
    public string key1 { get; set; }
    
    [SugarColumn(ColumnDescription = "备用字段2", IsNullable = true, Length = 64)]
    public string key2 { get; set; }
    
    [SugarColumn(ColumnDescription = "备用字段3", IsNullable = true, Length = 64)]
    public string key3 { get; set; }
    
    [SugarColumn(ColumnDescription = "备用字段4", IsNullable = true, Length = 64)]
    public string key4 { get; set; }
    
    [SugarColumn(ColumnDescription = "备用字段5", IsNullable = true, Length = 64)]
    public string key5 { get; set; }
    
    // /// <summary>
    // /// 排序号
    // /// </summary>
    // [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    // public int ornum { get; set; }

}