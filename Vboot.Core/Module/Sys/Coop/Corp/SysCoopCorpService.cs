﻿using Vboot.Core.Module.Bpm;

namespace Vboot.Core.Module.Sys;

public class SysCoopCorpService : BaseMainService<SysCoopCorp>, ITransient
{
    
    //级联新增
    public async Task<string> Insert(SysCoopCorp main)
    {
        if (string.IsNullOrEmpty(main.id))
        {
            main.id = YitIdHelper.NextId() + "";
        }

        if (string.IsNullOrEmpty(main.crmid))
        {
            main.crmid = XuserUtil.getUserId();
        }
        await repo.Context.Insertable(main).ExecuteCommandAsync();
        SysOrg sysOrg = new SysOrg(main.id, main.name, 128);
        await _orgRepo.InsertAsync(sysOrg);
        return main.id;
    }

    //级联修改
    public async Task<string> Update(SysCoopCorp main)
    {
        main.upmid = XuserUtil.getUserId();
        main.uptim = DateTime.Now;
        
        var oldCateId = await repo.Context.Queryable<SysCoopCorp>()
            .Where(it => it.id == main.id).Select(it => it.catid).SingleAsync();
        
        var oldCateTier = await repo.Context.Queryable<SysCoopCate>()
            .Where(it => it.id == oldCateId).Select(it => it.tier).SingleAsync();
        
        var newCateTier = await repo.Context.Queryable<SysCoopCate>()
            .Where(it => it.id == main.catid).Select(it => it.tier).SingleAsync();
        
        
        await repo.Context.Updateable(main).ExecuteCommandAsync();
        SysOrg sysOrg = new SysOrg(main.id, main.name, 128);
        await _orgRepo.UpdateAsync(sysOrg);
        if (oldCateTier != newCateTier)
        {
            await DealUserTier(oldCateTier+main.id+"x", newCateTier+main.id+"x");  
        }
        return main.id;
    }
    
    private async Task DealUserTier(string oldTier, string newTier)
    {
        var idNameList = await
            repo.Context.Ado.SqlQueryAsync<ZidName>(
                "select id,tier as name from sys_coop_user where tier like @oldTier",
                new {oldTier = oldTier + "%"});

        var list = new List<Dictionary<string, object>>();
        foreach (var idName in idNameList)
        {
            var dt = new Dictionary<string, object>
            {
                {"id", idName.id}, {"tier", idName.name.Replace(oldTier, newTier)}
            };
            list.Add(dt);
        }
        
        await repo.Context.Updateable(list).AS("sys_coop_user").WhereColumns("id").ExecuteCommandAsync();
    }

    //级联删除
    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        for (int i = 0; i < idArr.Length; i++)
        {
            var id = idArr[i];
            await repo.Context.Deleteable<SysCoopCorp>(it => it.id == id)
                .ExecuteCommandAsync();
        }
        await repo.Context.Deleteable<SysOrg>().In(ids).ExecuteCommandAsync();
    }

    //级联查询
    public async Task<SysCoopCorp> Select(string id)
    {
        var main = await repo.Context.Queryable<SysCoopCorp>()
            .Where(it => it.id == id)
            .Includes(t => t.crman)
            .Includes(t => t.upman)
            .SingleAsync();
        return main;
    }

    private SqlSugarRepository<SysOrg> _orgRepo;
    
    public SysCoopCorpService(SqlSugarRepository<SysCoopCorp> repo,
        SqlSugarRepository<SysOrg> orgRepo)
    {
        this.repo = repo;
        _orgRepo = orgRepo;
    }
}