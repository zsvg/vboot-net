﻿namespace Vboot.Core.Module.Sys;

/// <summary>
/// 协同分类
/// </summary>
[SugarTable("sys_coop_cate", TableDescription = "协同分类")]
public class SysCoopCate : BaseCateEntity
{
    /// <summary>
    /// 子分类集合
    /// </summary>
    [SugarColumn(IsIgnore = true)] 
    public List<SysCoopCate> children { get; set; }
    
}