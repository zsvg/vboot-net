﻿namespace Vboot.Core.Module.Ass;

[SugarTable("ass_addr_main", TableDescription = "省市区县")]
[Description("省市区县")]
public class AssAddrMain : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "父ID", IsNullable = true, Length = 32)]
    public string pid { get; set; }
    
    [SugarColumn(ColumnDescription = "经纬度", IsNullable = true, Length = 32)]
    public string cecoo { get; set; }
    
    [SugarColumn(ColumnDescription = "类型", IsNullable = true, Length = 32)]
    public string type { get; set; }

    [SugarColumn(ColumnDescription = "城市编码", IsNullable = true, Length = 32)]
    public string code { get; set; }

}