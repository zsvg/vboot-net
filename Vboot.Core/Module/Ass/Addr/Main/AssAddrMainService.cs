﻿namespace Vboot.Core.Module.Ass;

public class AssAddrMainService : BaseMainService<AssAddrMain>, ITransient
{
    public AssAddrMainService(SqlSugarRepository<AssAddrMain> repo)
    {
        this.repo = repo;
    }
}