﻿namespace Vboot.Core.Module.Ass;

public class AssDictCateService : BaseService<AssDictCate>, ITransient
{
    public AssDictCateService(SqlSugarRepository<AssDictCate> repo)
    {
        base.repo = repo;
    }
}