﻿namespace Vboot.Core.Module.Ass;

public class AssDictMainService : BaseMainService<AssDictMain>, ITransient
{
    public AssDictMainService(SqlSugarRepository<AssDictMain> repo)
    {
        this.repo = repo;
    }
}