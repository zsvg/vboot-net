﻿namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "字典配置")]
public class AssDictDataApi : IDynamicApiController
{
    private readonly AssDictDataService _service;

    public AssDictDataApi(AssDictDataService service)
    {
        _service = service;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string maiid, string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssDictData>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t 
                => t.name.Contains(name.Trim())||t.code.Contains(name.Trim()))
            .Where((t) => t.maiid == maiid)
            .OrderBy(u => u.ornum)
            .Select((t) => new {t.id, t.name, t.code,  t.crtim,t.uptim,t.notes})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }
    
    [QueryParameters]
    public async Task<dynamic> GetList(string maiid)
    {
        return await _service.repo.Context.Queryable<AssDictData>()
            .Where(it => it.maiid == maiid).ToListAsync();
    }

    public async Task<AssDictData> GetOne(string id)
    {
        var data = await _service.repo.Context.Queryable<AssDictData>()
            .Where(it => it.id == id).FirstAsync();
        return data;
    }

    public async Task Post(AssDictData data)
    {
        await _service.InsertAsync(data);
    }

    public async Task Put(AssDictData data)
    {
        await _service.UpdateAsync(data);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}