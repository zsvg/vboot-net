﻿namespace Vboot.Core.Module.Ass;

public class AssCogeFormService : BaseMainService<AssCogeForm>, ITransient
{
    public AssCogeFormService(SqlSugarRepository<AssCogeForm> repo)
    {
        this.repo = repo;
    }
}