﻿namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "表单设计")]
public class AssCogeFormApi : IDynamicApiController
{
    private readonly AssCogeFormService _service;

    public AssCogeFormApi(AssCogeFormService service)
    {
        _service = service;
    }
    
    public async Task<dynamic> GetList()
    {
        return await _service.repo.GetListAsync();
    }

    [QueryParameters]
    public async Task<dynamic> Get( string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssCogeForm>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t 
                => t.name.Contains(name.Trim())||t.id.Contains(name.Trim()))
            .Select((t) => new {t.id, t.name, t.crtim,t.uptim, t.notes})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<AssCogeForm> GetOne(string id)
    {
        var main = await _service.repo.Context.Queryable<AssCogeForm>()
            .Where(it => it.id == id).FirstAsync();
        return main;
    }

    public async Task Post(AssCogeForm main)
    {
        await _service.InsertAsync(main);
    }

    public async Task Put(AssCogeForm main)
    {
        await _service.UpdateAsync(main);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}