﻿namespace Vboot.Core.Module.Ass;

[SugarTable("ass_coge_form", TableDescription = "表单设计表")]
[Description("表单设计表")]
public class AssCogeForm : BaseMainEntity
{

    [SugarColumn(ColumnDescription = "vform", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string vform { get; set; }
}