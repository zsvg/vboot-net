﻿namespace Vboot.Core.Module.Ass;

/// <summary>
/// 文件存储配置
/// </summary>
public sealed class OssOptions : IConfigurableOptions
{
    public string AccessKeyId { get; set; }
    
    public string AccessKeySecret { get; set; }
    
    public string Endpoint { get; set; }
    
    public string Bucket { get; set; }
    
}