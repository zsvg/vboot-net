﻿namespace Vboot.Core.Module.Ass;

public class AssFormTempService : BaseMainService<AssFormTemp>, ITransient
{
    public AssFormTempService(SqlSugarRepository<AssFormTemp> repo)
    {
        this.repo = repo;
    }
}