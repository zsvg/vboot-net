﻿using Vboot.Core.Module.Sys;

namespace Vboot.Core.Module.Gen;

[ApiDescriptionSettings("Gen")]
public class GenCoopApi : IDynamicApiController
{
    private readonly SqlSugarRepository<SysCoopUser> _repo;

    public GenCoopApi(SqlSugarRepository<SysCoopUser> repo)
    {
        _repo = repo;
    }
    
    [QueryParameters]
    public async Task<dynamic> GetTree()
    {
        var treeList =await  _repo.Context.SqlQueryable<Ztree>
                ("select id,pid,name from sys_coop_cate " +
                 "union all select id,catid as pid,name from sys_coop_corp")
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }
    
    [QueryParameters]
    public async Task<dynamic> GetList(string pid,int type)
    {
        if (string.IsNullOrEmpty(pid)&&(type & 1) != 0)
        {
            return await  _repo.Context.SqlQueryable<ZidName>
                    ("select id,name from sys_coop_cate where pid is null order by ornum")
                .ToListAsync();
        }
        List<dynamic> list = new List<dynamic>();
        if ((type & 1) != 0)
        {
            var cateList =await  _repo.Context.SqlQueryable<ZidName>
                    ("select id,name from sys_coop_cate where pid=@pid order by ornum")
                .AddParameters(new SugarParameter[] {new SugarParameter("@pid", pid)})
                .ToListAsync();
            list.AddRange(cateList);
        }
        if ((type & 2) != 0)
        {
            var cateList =await  _repo.Context.SqlQueryable<ZidName>
                    ("select id,name from sys_coop_corp where catid=@catid")
                .AddParameters(new SugarParameter[] {new SugarParameter("@catid", pid)})
                .ToListAsync();
            list.AddRange(cateList);
        }
        if ((type & 4) != 0)
        {
            var cateList =await  _repo.Context.SqlQueryable<ZidName>
                    ("select id,name from sys_coop_user where corid=@corid order by ornum")
                .AddParameters(new SugarParameter[] {new SugarParameter("@corid", pid)})
                .ToListAsync();
            list.AddRange(cateList);
        }
        return list;
      
    }
    

}