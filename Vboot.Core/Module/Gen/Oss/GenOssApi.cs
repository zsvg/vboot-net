﻿using Vboot.Core.Module.Ass;

namespace Vboot.Core.Module.Gen;

[ApiDescriptionSettings("Gen")]
public class GenOssApi : IDynamicApiController
{
    
    private readonly AssOssMainService _ossService;

    public GenOssApi(AssOssMainService ossService)
    {
        _ossService = ossService;
    }

    [NonUnify]
    public async Task<Zfile> PostUpload(IFormFile file)
    {
        var sysFile = await _ossService.UploadFile(file);
        return sysFile;
    }
    
    [QueryParameters]
    public IActionResult GetDownload(string table,string id)
    {
        return _ossService.DownloadFile(table,id);
    }
    
    [QueryParameters]
    public AssOssMain GetInfo(string id)
    {
        return _ossService.GetInfo(id);
    }
    
    [QueryParameters]
    public List<AssOssMain> GetInfos(string ids)
    {
        return _ossService.GetInfos(ids);
    }
    
    
}