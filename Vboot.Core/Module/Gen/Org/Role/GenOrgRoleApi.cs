﻿using Vboot.Core.Module.Sys;

namespace Vboot.Core.Module.Gen;

[ApiDescriptionSettings("Gen")]
public class GenOrgRoleApi : IDynamicApiController
{
    private readonly SqlSugarRepository<SysOrgRoleTree> _repo;

    public GenOrgRoleApi(SqlSugarRepository<SysOrgRoleTree> repo)
    {
        _repo = repo;
    }
    
    [QueryParameters]
    public async Task<dynamic> GetTree()
    {
        var treeList =await  _repo.Context.SqlQueryable<Ztree>
                ("select id,name,null pid from sys_org_role_tree order by ornum")
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }
    
    [QueryParameters]
    public async Task<dynamic> GetList(string treid,string name)
    {
        var list =await  _repo.Context.SqlQueryable<ZidName>
                ("select id,name from sys_org_role where treid=@treid order by ornum")
            .AddParameters(new SugarParameter[] {new SugarParameter("@treid", treid)})
            .ToListAsync();
        return list;
    }
    

}