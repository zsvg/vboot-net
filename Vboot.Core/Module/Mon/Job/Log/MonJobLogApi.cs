﻿namespace Vboot.Core.Module.Mon;

[ApiDescriptionSettings("Mon", Tag = "定时任务-日志")]
public class MonJobLogApi : IDynamicApiController
{
    private readonly MonJobLogService _service;

    public MonJobLogApi(MonJobLogService monJobLogService)
    {
        _service = monJobLogService;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<MonJobLog>()
            .Select((t) => new {t.id, t.name, t.sttim, t.entim, t.msg, t.ret})
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<MonJobLog> GetOne(string id)
    {
        var cate = await _service.SingleAsync(id);
        return cate;
    }

    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        await _service.DeleteAsync(idArr);
    }
}