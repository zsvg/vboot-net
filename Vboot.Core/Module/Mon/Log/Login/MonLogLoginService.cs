﻿namespace Vboot.Core.Module.Mon;

public class MonLogLoginService : BaseService<MonLogLogin>, ITransient
{
    public MonLogLoginService(SqlSugarRepository<MonLogLogin> repo)
    {
        this.repo = repo;
    }
}