﻿namespace Vboot.Core.Framework;

/// <summary>
/// SqlSugar相关常量
/// </summary>
public class SqlSugarConst
{
    /// <summary>
    /// 默认数据库标识
    /// </summary>
    public const string ConfigId = "Vboot";

    /// <summary>
    /// 默认表主键
    /// </summary>
    public const string PrimaryKey = "id";
}