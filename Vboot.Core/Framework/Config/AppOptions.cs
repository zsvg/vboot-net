﻿namespace Vboot.Core.Framework;

public sealed class AppOptions : IConfigurableOptions
{
    public bool DbFirstForCore  { get; set; }
    
    public bool DbFirstForExtend  { get; set; }
    
    public bool DbFirstForApp  { get; set; }
    
    public bool ShowSql  { get; set; }
    
}