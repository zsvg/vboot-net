﻿namespace Vboot.Core.Framework.Security;

//url扫描收集时是用到的pojo
public class Yurl
{
    public string id { get; set; }
    
    public string url { get; set; }
    
    public string pid { get; set; }
    
    public string type { get; set; }
}