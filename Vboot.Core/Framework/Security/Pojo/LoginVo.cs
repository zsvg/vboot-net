﻿namespace Vboot.Core.Framework.Security;


//用户名与密码VO，登录时用到
public class LoginVo
{
    /// <example>sa</example>
    public string username { get; set; }

    /// <example>1</example>
    public string password { get; set; }
}