﻿namespace Vboot.App.My;

/// <summary>
/// DEMO分类
/// </summary>
[SugarTable("my_demo_cate", TableDescription = "DEMO分类")]
public class MyDemoCate : BaseCateEntity
{
    /// <summary>
    /// 子分类集合
    /// </summary>
    [SugarColumn(IsIgnore = true)] 
    public List<MyDemoCate> children { get; set; }
    
    [SugarColumn(ColumnDescription = "表单形式", IsNullable = true, Length = 32)]
    public string fomod { get; set; }

    [SugarColumn(ColumnDescription = "是否有流程", IsNullable = true)]
    public bool prtag{ get; set; }

    [SugarColumn(ColumnDescription = "全局流程模板ID", IsNullable = true, Length = 32)]
    public string protd{ get; set; }
    
    [SugarColumn(IsIgnore = true)] public string prxml { get; set; }

    [SugarColumn(ColumnDescription = "表单字段", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string fjson { get; set; }
    
    [SugarColumn(ColumnDescription = "表单页签", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string tjson { get; set; }
    
    [SugarColumn(ColumnDescription = "表单联动", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string ljson { get; set; }

    [Navigate(typeof(MyDemoCateUsman), nameof(MyDemoCateUsman.mid), nameof(MyDemoCateUsman.oid))]
    public List<SysOrg> usmen { get; set; } 
    //
    [Navigate(typeof(MyDemoCateReman), nameof(MyDemoCateReman.mid), nameof(MyDemoCateReman.oid))]
    public List<SysOrg> remen { get; set; }
    
}