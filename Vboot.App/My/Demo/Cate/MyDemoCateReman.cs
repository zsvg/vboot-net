﻿namespace Vboot.App.My;

/// <summary>
/// DEMO分类数据可阅读者
/// </summary>
[SugarTable("my_demo_cate_reman", TableDescription = "数据可阅读者")]
public class MyDemoCateReman
{
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "DEMO ID")]
    public string mid{ get; set; }
        
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "可阅读者ID")]
    public string oid{ get; set; }
}