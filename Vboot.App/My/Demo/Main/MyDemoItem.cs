﻿namespace Vboot.App.My;

/// <summary>
/// DEMO明细行---主记录OneToMany
/// </summary>
[SugarTable("my_demo_item", TableDescription = "DEMO明细行")]
public class MyDemoItem
{
    /// <summary>
    /// 主键
    /// </summary>
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true, Length = 36)]
    public string id { get; set; }
    
    /// <summary>
    /// 姓名
    /// </summary>
    [SugarColumn(ColumnDescription = "姓名", IsNullable = true, Length = 36)]
    public string name { get; set; }
    
    /// <summary>
    /// 手机号
    /// </summary>
    [SugarColumn(ColumnDescription = "手机号", IsNullable = true, Length = 32)]
    public string monum { get; set; }
        
    /// <summary>
    /// 性别
    /// </summary>
    [SugarColumn(ColumnDescription = "性别", IsNullable = true, Length = 32)]
    public string gender { get; set; }
    
    /// <summary>
    /// 职务
    /// </summary>
    [SugarColumn(ColumnDescription = "职务", IsNullable = true, Length = 32)]
    public string post { get; set; }
    
    /// <summary>
    /// 邮箱
    /// </summary>
    [SugarColumn(ColumnDescription = "邮箱", IsNullable = true, Length = 32)]
    public string email { get; set; }
    
    /// <summary>
    /// 备注
    /// </summary>
    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 255)]
    public string notes { get; set; }
    
    /// <summary>
    /// 排序号
    /// </summary>
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }
    
    /// <summary>
    /// DEMO ID
    /// </summary>
    [SugarColumn(ColumnDescription = "DEMO ID", IsNullable = true, Length = 36)]
    public string maiid { get; set; }
    
}