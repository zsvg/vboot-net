using Vboot.Core.Module.Bpm;

namespace Vboot.App.My;

/// <summary>
/// DEMO主信息接口
/// </summary>
public class MyDemoMainApi : IDynamicApiController
{
    private readonly MyDemoMainService _service;

    private readonly AssNumMainService _numService;
    
    private readonly BpmTaskMainService _taskMainService;


    public MyDemoMainApi(MyDemoMainService service,
        AssNumMainService numService,
        BpmTaskMainService taskMainService)
    {
        _service = service;
        _numService = numService;
        _taskMainService = taskMainService;
    }

    /// <summary>
    /// 获取DEMO主信息的分页数据
    /// </summary>
    /// <returns></returns>
    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {

        var pp = XreqUtil.GetPp();
        var items = new List<dynamic>();
        
        // items = await _service.repo.Context.Queryable<MyDemoMain, OaFlowTemp, SysOrg>((t, c, o)
        //         => new JoinQueryInfos(JoinType.Left, c.id == t.temid, JoinType.Left, o.id == t.crmid))
        //     .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
        //     .OrderBy(t => t.crtim, OrderByType.Desc)
        //     .Select((t, c, o) =>
        //         (dynamic) new {t.id, t.name, t.notes, temna = c.name, crman = o.name, t.crtim, t.uptim, t.state})
        //     .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        
       items  = await _service.repo.Context.Queryable<MyDemoMain, SysOrg, SysOrg,MyDemoCate>
            ((t, o, o2,c) => new JoinQueryInfos(
                JoinType.Left, o.id == t.crmid,
                JoinType.Left, o2.id == t.upmid,
                JoinType.Left, c.id == t.catid))
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(t => t.crtim, OrderByType.Desc)
            .Select((t, o, o2,c)
                =>  (dynamic) new {t.id, t.name, t.state, t.addre,t.notes, t.crtim, t.uptim, t.senum, crman = o.name, upman = o2.name,catna=c.name,type=c.fomod})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        await _taskMainService.FindCurrentExmen(items);
        return RestPageResult.Build(pp.total.Value, items);
    }

    /// <summary>
    /// 获取单个DEMO的详细信息
    /// </summary>
    /// <param name="id">DEMO ID</param>
    /// <returns></returns>
    [Oplog("DEMO详情查询")]
    public async Task<MyDemoMain> GetOne(string id)
    {
        return await _service.Select(id);
    }

    /// <summary>
    /// 新增DEMO
    /// </summary>
    // [MyUnitOfWork]
    public async Task<string> Post(MyDemoMain main)
    {
        main.senum = _numService.getNum("DEMO");
        if (string.IsNullOrEmpty(main.protd))
        {
            main.state = "30";
            return await _service.Insert(main);
        }
        else
        {
            return await _service.Insertx(main);
        }
    }

    /// <summary>
    /// 修改DEMO
    /// </summary>
    [MyUnitOfWork]
    public async Task<string> Put(MyDemoMain main)
    {
        if (string.IsNullOrEmpty(main.protd))
        {
            return await _service.Update(main);
        }
        else
        {
            return await _service.Updatex(main);
        }
    }

    /// <summary>
    /// 删除DEMO
    /// </summary>
    [MyUnitOfWork]
    public async Task Delete(string ids)
    {
        await _service.Delete(ids);
    }
}