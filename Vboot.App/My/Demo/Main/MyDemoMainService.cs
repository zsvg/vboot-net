﻿using Furion.JsonSerialization;
using Vboot.Core.Module.Bpm;

namespace Vboot.App.My;

//DEMO服务
public class MyDemoMainService : BaseMainService<MyDemoMain>, ITransient
{
    public async Task<string> Insertx(MyDemoMain main)
    {
        main.state = "20";
        main.uptim = DateTime.Now;
        await Insert(main);
        
        BpmProcCond cond = new BpmProcCond();
        cond.id=main.id;
        cond.cond=_jsonSerializer.Serialize(main);
        await _condRepo.InsertAsync(cond);
        
        Zbpm zbpm = main.zbpm;
        zbpm.modty = "app_demo";
        zbpm.proid = main.id;
        zbpm.prona = main.name;
        zbpm.haman = main.crmid;
        zbpm.temid = main.protd;
        Znode znode = await _procService.Start(zbpm);
        if (znode.facno == "NE")
        {
            main.state = "30";
            await UpdateAsync(main);
        }
        return main.id;
    }
    
    //级联新增
    public async Task<string> Insert(MyDemoMain main)
    {
        main.id = YitIdHelper.NextId() + "";
        main.crmid = XuserUtil.getUserId();
        await repo.Context.InsertNav(main)
            .Include(it => it.vimen)
            .Include(it => it.edmen)
            .Include(it => it.items)
            .Include(it => it.atts)
            .ExecuteCommandAsync();
        return main.id;
    }
    
    public async Task<string> Updatex(MyDemoMain main)
    {
        await Update(main);
        BpmProcCond cond = new BpmProcCond();
        cond.id=main.id;
        cond.cond=_jsonSerializer.Serialize(main);
        await _condRepo.UpdateAsync(cond);
        
        main.zbpm.modty = "app_demo";
        
        if (main.zbpm.opkey == "pass")
        {
            Znode znode =await _procService.HandlerPass(main.zbpm);
            if (znode.facno == "NE")
            {
                main.state = "30";
                await UpdateAsync(main);
            }
            else
            {
                main.state = "20";
                await UpdateAsync(main);
            }
        }
        else if (main.zbpm.opkey == "refuse")
        {
            if ("N1"==main.zbpm.tarno) {
                main.state="11";
                await UpdateAsync(main);
            }
            await _procService.HandlerRefuse(main.zbpm);
        }else if (main.zbpm.opkey == "turn") {
            await _procService.handlerTurn(main.zbpm);
        } else if (main.zbpm.opkey == "communicate") {
            await _procService.handlerCommunicate(main.zbpm);
        } else if (main.zbpm.opkey == "abandon") {
            await _procService.handlerAbandon(main.zbpm);
            main.state="00";
            await UpdateAsync(main);
        }else if (main.zbpm.opkey == "bacommunicate") {
            await _procService.handlerBacommunicate(main.zbpm);
        }
        
        return main.id;
    }

    //级联修改
    public async Task<string> Update(MyDemoMain main)
    {
        main.upmid = XuserUtil.getUserId();
        await repo.Context.UpdateNav(main)
            .Include(it => it.vimen)
            .Include(it => it.edmen)
            .Include(it => it.items)
            .Include(it => it.atts)
            .ExecuteCommandAsync();
        return main.id;
    }

    //级联删除
    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        for (int i = 0; i < idArr.Length; i++)
        {
            var id = idArr[i];
            await repo.Context.DeleteNav<MyDemoMain>(it => it.id == id)
                .Include(it => it.vimen, new DeleteNavOptions() {ManyToManyIsDeleteA = true})
                .Include(it => it.edmen, new DeleteNavOptions() {ManyToManyIsDeleteA = true})
                .Include(it => it.items)
                .Include(it => it.atts)
                .ExecuteCommandAsync();
        }
    }

    //级联查询
    public async Task<MyDemoMain> Select(string id)
    {
        var main = await repo.Context.Queryable<MyDemoMain>()
            .Where(it => it.id == id)
            .Includes(t => t.crman)
            .Includes(t => t.upman)
            .Includes(t => t.vimen)
            .Includes(t => t.edmen)
            .Includes(t => t.items.OrderBy(b => b.ornum).ToList())
            .Includes(t => t.atts.OrderBy(b => b.ornum).ToList())
            .Includes(t => t.opman)
            .SingleAsync();
        return main;
    }

    private readonly BpmProcMainService _procService;
    
    private readonly SqlSugarRepository<BpmProcCond> _condRepo;
  
    private readonly IJsonSerializerProvider _jsonSerializer;

    public MyDemoMainService(SqlSugarRepository<MyDemoMain> repo,
        BpmProcMainService procService,
        SqlSugarRepository<BpmProcCond> condRepo,
        IJsonSerializerProvider jsonSerializer)
    {
        this.repo = repo;
        _procService = procService;
        _condRepo = condRepo;
        _jsonSerializer = jsonSerializer;
    }
}