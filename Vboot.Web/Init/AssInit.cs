﻿using Furion.DependencyInjection;
using Vboot.Core.Framework;
using Vboot.Core.Module.Ass;
using Yitter.IdGenerator;

namespace Vboot.Web.Init;

//辅助数据初始化
public class AssInit : ITransient
{
    private readonly SqlSugarRepository<AssNumMain> _numRepo;

    private readonly SqlSugarRepository<AssDictMain> _dictMainRepo;

    private readonly SqlSugarRepository<AssDictData> _dictDataRepo;

    public AssInit(SqlSugarRepository<AssNumMain> numRepo,
        SqlSugarRepository<AssDictMain> dictMainRepo,
        SqlSugarRepository<AssDictData> dictDataRepo)
    {
        _numRepo = numRepo;
        _dictMainRepo = dictMainRepo;
        _dictDataRepo = dictDataRepo;
    }

    public async Task InitData()
    {
        await InitNum();
        await InitDict();
    }

    //流水号初始化
    private async Task InitNum()
    {
        List<AssNumMain> numList = new List<AssNumMain>();
        AssNumMain demoNum = new AssNumMain();
        demoNum.id = "DEMO";
        demoNum.name = "DEMO流水号";
        demoNum.numod = "yy";
        demoNum.nulen = 4;
        demoNum.nflag = true;
        demoNum.nupre = "D";
        numList.Add(demoNum);

        AssNumMain projNum = new AssNumMain();
        projNum.id = "PROJ";
        projNum.name = "项目流水号";
        projNum.numod = "yyyymmdd";
        projNum.nulen = 3;
        projNum.nflag = true;
        projNum.nupre = "PR";
        numList.Add(projNum);

        AssNumMain custNum = new AssNumMain();
        custNum.id = "CUST";
        custNum.name = "客户流水号";
        custNum.numod = "yyyymmdd";
        custNum.nulen = 3;
        custNum.nflag = true;
        custNum.nupre = "CU";
        numList.Add(custNum);

        AssNumMain distNum = new AssNumMain();
        distNum.id = "DIST";
        distNum.name = "渠道商流水号";
        distNum.numod = "nodate";
        distNum.nulen = 7;
        distNum.nflag = true;
        distNum.nupre = "DMS";
        distNum.nunex = "1000001";
        numList.Add(distNum);
        
        AssNumMain distUserNum = new AssNumMain();
        distUserNum.id = "DIST_USER";
        distUserNum.name = "渠道商流水号";
        distUserNum.numod = "nodate";
        distUserNum.nulen = 6;
        distUserNum.nflag = true;
        distUserNum.nupre = "d";
        distUserNum.nunex = "100001";
        numList.Add(distUserNum);
        
        await _numRepo.InsertRangeAsync(numList);
        
  
    }

    //数据字典初始化
    private async Task InitDict()
    {
        List<AssDictMain> dictList = new List<AssDictMain>();

        AssDictMain demoDict = new AssDictMain();
        demoDict.id = "DEMO_GRADE";
        demoDict.name = "DEMO资质等级";
        demoDict.avtag = true;
        dictList.Add(demoDict);

        AssDictMain distGradeDict = new AssDictMain();
        distGradeDict.id = "DIST_GRADE";
        distGradeDict.name = "渠道商资质等级";
        distGradeDict.avtag = true;
        dictList.Add(distGradeDict);

        await _dictMainRepo.InsertRangeAsync(dictList);

        List<AssDictData> dictDataList = new List<AssDictData>();
        AssDictData demo1 = new AssDictData();
        demo1.id = YitIdHelper.NextId() + "";
        demo1.code = "A";
        demo1.name = "A级资质";
        demo1.avtag = true;
        demo1.ornum = 1;
        demo1.maiid = "DEMO_GRADE";
        dictDataList.Add(demo1);

        AssDictData demo2 = new AssDictData();
        demo2.id = YitIdHelper.NextId() + "";
        demo2.code = "B";
        demo2.name = "B级资质";
        demo2.avtag = true;
        demo2.ornum = 2;
        demo2.maiid = "DEMO_GRADE";
        dictDataList.Add(demo2);

        AssDictData demo3 = new AssDictData();
        demo3.id = YitIdHelper.NextId() + "";
        demo3.code = "C";
        demo3.name = "C级资质";
        demo3.avtag = true;
        demo3.ornum = 3;
        demo3.maiid = "DEMO_GRADE";
        dictDataList.Add(demo3);

        AssDictData demo4 = new AssDictData();
        demo4.id = YitIdHelper.NextId() + "";
        demo4.code = "Z";
        demo4.name = "不合格";
        demo4.avtag = true;
        demo4.ornum = 4;
        demo4.maiid = "DEMO_GRADE";
        dictDataList.Add(demo4);

        AssDictData dist1 = new AssDictData();
        dist1.id = YitIdHelper.NextId() + "";
        dist1.code = "A";
        dist1.name = "A级资质";
        dist1.avtag = true;
        dist1.ornum = 1;
        dist1.maiid = "DIST_GRADE";
        dictDataList.Add(dist1);

        AssDictData dist2 = new AssDictData();
        dist2.id = YitIdHelper.NextId() + "";
        dist2.code = "B";
        dist2.name = "B级资质";
        dist2.avtag = true;
        dist2.ornum = 2;
        dist2.maiid = "DIST_GRADE";
        dictDataList.Add(dist2);

        AssDictData dist3 = new AssDictData();
        dist3.id = YitIdHelper.NextId() + "";
        dist3.code = "C";
        dist3.name = "C级资质";
        dist3.avtag = true;
        dist3.ornum = 3;
        dist3.maiid = "DIST_GRADE";
        dictDataList.Add(dist3);

        AssDictData dist4 = new AssDictData();
        dist4.id = YitIdHelper.NextId() + "";
        dist4.code = "Z";
        dist4.name = "不合格";
        dist4.avtag = true;
        dist4.ornum = 4;
        dist4.maiid = "DIST_GRADE";
        dictDataList.Add(dist4);

        await _dictDataRepo.InsertRangeAsync(dictDataList);
    }
}